import cv2
import socket
import time
import numpy as np
from PIL import Image
from io import StringIO

host, port = '192.168.1.191', 9999
addr = (host, port)
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
sock.connect(addr)

for i in range(10):
    cap = cv2.VideoCapture(i)
    ret, frame = cap.read()
    if ret:
        print('video port {} is correct.'.format(i))
        break
    print('video port {} is not correct.'.format(i))
if not ret:
    print('open camero failes. exit')
    exit()



while True:
    ret, frame = cap.read()
    print('ret = {}'.format(ret))
    pi = Image.fromarray(np.uint8(frame))
    buf = StringIO()
    pi.save(buf,format='JPEG')
    jpeg = buf.getvalue()
    buf.close()
    sock.sendall(jpeg)
    break

cap.release()
sock.close()
